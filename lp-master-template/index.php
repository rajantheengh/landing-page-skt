<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
	<title>Landing Page Master</title>
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<header id="header" class="site-header">
		<div class="site-logo">
			<img src="" alt="">
		</div> <!-- / .site-logo -->

		<div class="phone-number">
			<a href="tel:+18009086679">800.908.6679</a>
		</div> <!-- / .phone-number -->
	</header> <!-- / .site-header -->

	<main id="main" class="site-content">

		<form method="POST" action="mailer/index.php" id="form-id-0" class="validate form">
			<div class="form-header">
				<h2>Form Heading</h2>
				<p>Form basic info text goes heres if available</p>
			</div>
			<div class="form-body form-section-0" >
				<div class="form-group">
				<input class="form-control" placeholder="Name" name="name" type="text" value="">
				<div class="messages"></div>
			</div>
			<div class="form-group">
				<input class="form-control" placeholder="Phone" name="phone" type="text" value="">
				<div class="messages"></div>
			</div>
			<div class="form-group">
				<input class="form-control" placeholder="Email" name="email" type="email" value="">
				<div class="messages"></div>
			</div>
			<div class="form-group">
				<input class="form-control" placeholder="Zip Code*" name="zip-code" type="text" value="">
				<div class="messages"></div>
			</div>
			</div>
			<div class="form-footer">
				<button type="submit" class="submitButton btn btn-primary" ><span class="spinner-border spinner-border loader" aria-hidden="true" style="display: none"></span> Submit</button>
			</div>
		</form>


		<form method="POST" action="mailer/index.php" id="form-id-1" class="validate form">
			<div class="form-header">
				<h2>Form heading</h2>
			</div>
			<div class="form-section-0" >
				<div class="form-group">
					<input class="form-control" placeholder="Enter your newsletter now" name="enter-your-newsletter-now" type="email" value="">
					<div class="messages"></div>
				</div>
			</div>
			<div class="form-footer">
				<button type="submit" class="submitButton btn btn-primary" ><span class="spinner-border spinner-border loader" aria-hidden="true" style="display: none"></span> Submit</button>
			</div>
		</form>

	</main> <!-- / .site-content -->

	<footer id="footer" class="site-footer">
		<div class="container">
			<span class="copyright">Footer Copyright text goes here</span>
		</div>
	</footer> <!-- / .site-footer -->

	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/underscore-min.js"></script>
	<script src="assets/js/validate.min.js"></script>
	<script src="assets/js/builder.js"></script>
	<script src="assets/js/main.js"></script>

</body>
</html>